//
//  Photo+Flickr.h
//  FlickrSearch
//
//  Created by Matt Resmont on 7/3/15.
//  Copyright (c) 2015 Resimont Products INC. All rights reserved.
//

#import "Photo.h"

@interface Photo (Flickr)

+ (Photo *)photoWithFlickrInfo:(NSDictionary *)photoDictionary
        inManagedObjectContext:(NSManagedObjectContext *)context;

+ (void)loadPhotosFromFlickrArray:(NSArray *)photos
         intoManagedObjectContext:(NSManagedObjectContext *)context;

@end
