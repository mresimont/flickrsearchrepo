//
//  Photographer.m
//  FlickrSearch
//
//  Created by Matt Resmont on 7/3/15.
//  Copyright (c) 2015 Resimont Products INC. All rights reserved.
//

#import "Photographer.h"
#import "Photo.h"


@implementation Photographer

@dynamic name;
@dynamic photos;

@end
