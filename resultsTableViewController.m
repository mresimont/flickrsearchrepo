//
//  resultsTableViewController.m
//  FlickrCoreData
//
//  Created by Matt Resmont on 7/1/15.
//  Copyright (c) 2015 Resimont Products INC. All rights reserved.
//

#import "resultsTableViewController.h"
#import "Photo+Flickr.h"
#import "ImageViewController.h"

@interface resultsTableViewController ()

@end

@implementation resultsTableViewController

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
     UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Photo Cell"];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Photo Cell"];
    }
    
    Photo  *picture = [self.fetchedResultsController objectAtIndexPath:indexPath];
    if (![picture.title isEqualToString:@""])
    {
        cell.textLabel.text = [NSString stringWithFormat:@"Title: %@",picture.title];
    }else
    {
        cell.textLabel.text = [NSString stringWithFormat:@"No Title"];
    }
    cell.detailTextLabel.text = picture.subtitle.description;
     
    return cell;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
    
    if ([segue.identifier isEqualToString:@"Display Photo"])
    {
        if ([segue.destinationViewController isKindOfClass:[ImageViewController class]])
        {
            ImageViewController *ivc = segue.destinationViewController;
            Photo *picture = [self.fetchedResultsController objectAtIndexPath:indexPath];
            ivc.imageURL = [NSURL URLWithString:picture.imageURL];
            ivc.title = picture.title;
            
        }
    }
}


@end
