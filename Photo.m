//
//  Photo.m
//  FlickrSearch
//
//  Created by Matt Resmont on 7/3/15.
//  Copyright (c) 2015 Resimont Products INC. All rights reserved.
//

#import "Photo.h"
#import "Photographer.h"


@implementation Photo

@dynamic imageURL;
@dynamic subtitle;
@dynamic tags;
@dynamic title;
@dynamic unique;
@dynamic whoTook;

@end
