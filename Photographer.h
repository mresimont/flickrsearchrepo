//
//  Photographer.h
//  FlickrSearch
//
//  Created by Matt Resmont on 7/3/15.
//  Copyright (c) 2015 Resimont Products INC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Photo;

@interface Photographer : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) Photo *photos;

@end
