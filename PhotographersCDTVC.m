//
//  PhotographersCDTVC.m
//  FlickrCoreData
//
//  Created by Matt Resmont on 6/26/15.
//  Copyright (c) 2015 Resimont Products INC. All rights reserved.
//

#import "PhotographersCDTVC.h"
#import "Photographer+Create.h"
#import "Photo+Flickr.h"
#import "PhotoDatabaseAvailability.h"
#import "ImageViewController.h"
#import "resultsTableViewController.h"
#import "FlickrFetcher.h"

@interface PhotographersCDTVC()<UISearchBarDelegate, UISearchControllerDelegate, UISearchResultsUpdating>

@property (nonatomic, strong) UISearchController *searchController;
@property (nonatomic, strong) resultsTableViewController *resultsTableController;
@property (nonatomic, strong) Photo *photo;
@property BOOL searchControllerWasActive;
@property BOOL searchControllerSearchFieldWasFirstResponder;

@end

@implementation PhotographersCDTVC




//sets the mangedObjectContext and fetches the photos and
//saves them in the CoreDataTableViewController's fetchedResultsConroller and
//sets the navigationBar's title
- (void)setManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
   
    _managedObjectContext = managedObjectContext;
    self.fetchedResultsController = [self fetchPhotosWithSearchOption:@" "];
    self.navigationItem.title = [NSString stringWithFormat:@"%lu photos",(unsigned long)self.fetchedResultsController.fetchedObjects.count];
    
}




//updates the navigation bar's title

- (void)updateNavigationTitle:(NSTimer *)timer
{
     self.navigationItem.title = [NSString stringWithFormat:@"%lu photos",(unsigned long)self.fetchedResultsController.fetchedObjects.count];
}






//method to fetch the wanted data from the Photo table and returns it as a NSFetchedResultsController

- (NSFetchedResultsController *)fetchPhotosWithSearchOption:(NSString *)search
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Photo"];
    
    if (![search isEqualToString:@" "])
    {
        request.predicate = [NSPredicate predicateWithFormat:@"title CONTAINS %@ || subtitle CONTAINS %@ || tags CONTAINS %@",search, search, search, search];
    }else request.predicate = nil;
    
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"title"
                                                              ascending:YES
                                                               selector:@selector(localizedStandardCompare:)]];
    return [[NSFetchedResultsController alloc] initWithFetchRequest:request
                                               managedObjectContext:self.managedObjectContext
                                                 sectionNameKeyPath:nil
                                                          cacheName:nil];
}


//configures the tableView's cell with the data saved in the fetchedResultsController

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = (UITableViewCell *)[self.tableView dequeueReusableCellWithIdentifier:@"Photo Cell" forIndexPath:indexPath];
        Photo  *picture = [self.fetchedResultsController objectAtIndexPath:indexPath];
        if (![picture.title isEqualToString:@""])
        {
            cell.textLabel.text = [NSString stringWithFormat:@"Title: %@",picture.title];
        }else
        {
            cell.textLabel.text = [NSString stringWithFormat:@"No Title"];
        }
        cell.detailTextLabel.text = picture.subtitle;
    
   
    return cell;
}


//gets the info of the cell clicked the the segue can send it to the ImageViewController

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    resultsTableViewController *rtvc = (resultsTableViewController *)self.searchController.searchResultsController;
    self.photo = (tableView == self.tableView) ? [self.fetchedResultsController objectAtIndexPath:indexPath] : [rtvc.fetchedResultsController objectAtIndexPath:indexPath];
    
    [self performSegueWithIdentifier:@"Display Photo" sender:self];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

-(void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    NSString *searchText = searchController.searchBar.text;
    resultsTableViewController *rtvc = (resultsTableViewController *)self.searchController.searchResultsController;

    rtvc.fetchedResultsController = [self fetchPhotosWithSearchOption:searchText];
    
    [rtvc.tableView reloadData];
    
   
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

- (void) viewDidLoad
{
    _resultsTableController = [[resultsTableViewController alloc] init];
    _searchController = [[UISearchController alloc] initWithSearchResultsController:self.resultsTableController];
    self.searchController.searchResultsUpdater = self;
    [self.searchController.searchBar sizeToFit];
    self.tableView.tableHeaderView = self.searchController.searchBar;
    
    self.resultsTableController.tableView.delegate = self;
    self.searchController.delegate = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.searchBar.delegate = self;
    
    self.definesPresentationContext = YES;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    
    if (self.searchControllerWasActive)
    {
        _searchControllerWasActive = NO;
        
        if (self.searchControllerSearchFieldWasFirstResponder)
        {
            [self.searchController.searchBar becomeFirstResponder];
            _searchControllerSearchFieldWasFirstResponder = NO;
        }
    }
}

- (void) awakeFromNib
{
    [[NSNotificationCenter defaultCenter] addObserverForName:PhotoDatabaseAvailabilityNotification
                                                      object:nil
                                                       queue:nil
                                                  usingBlock:^(NSNotification *note) {
                                                      self.managedObjectContext = note.userInfo[PhotoDatabaseAvailabilityContext];
                                                      
                                                  }];
    
    [NSTimer scheduledTimerWithTimeInterval:5 * 60
                                     target:self
                                   selector:@selector(updateNavigationTitle:)
                                   userInfo:nil
                                    repeats:YES];

    
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
   

        if ([segue.identifier isEqualToString:@"Display Photo"])
        {
            if ([segue.destinationViewController isKindOfClass:[ImageViewController class]])
            {
                ImageViewController *ivc = segue.destinationViewController;

                ivc.imageURL = [NSURL URLWithString:self.photo.imageURL];
                ivc.title = self.photo.title;
                
            }
        }
}


@end
