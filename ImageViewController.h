//
//  ImageViewController.h
//  shutterbug
//
//  Created by Matt Resmont on 6/14/15.
//  Copyright (c) 2015 Resimont Products INC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageViewController : UIViewController

@property (strong, nonatomic) NSURL *imageURL;

@end
