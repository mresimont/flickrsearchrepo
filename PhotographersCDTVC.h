//
//  PhotographersCDTVC.h
//  FlickrCoreData
//
//  Created by Matt Resmont on 6/26/15.
//  Copyright (c) 2015 Resimont Products INC. All rights reserved.
//

#import "CoreDataTableViewController.h"

@interface PhotographersCDTVC : CoreDataTableViewController


@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@end
