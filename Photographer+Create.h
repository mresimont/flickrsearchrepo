//
//  Photographer+Create.h
//  FlickrSearch
//
//  Created by Matt Resmont on 7/3/15.
//  Copyright (c) 2015 Resimont Products INC. All rights reserved.
//

#import "Photographer.h"

@interface Photographer (Create)

+ (Photographer *)photographerWithName:(NSString *)name
                inManagedObjectContext:(NSManagedObjectContext *)context;

@end
