//
//  resultsTableViewController.h
//  FlickrCoreData
//
//  Created by Matt Resmont on 7/1/15.
//  Copyright (c) 2015 Resimont Products INC. All rights reserved.
//

#import "CoreDataTableViewController.h"

@interface resultsTableViewController : CoreDataTableViewController

@property (nonatomic, strong) NSArray *array;

@end
