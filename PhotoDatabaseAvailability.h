//
//  PhotoDatabaseAvailability.h
//  FlickrCoreData
//
//  Created by Matt Resmont on 6/26/15.
//  Copyright (c) 2015 Resimont Products INC. All rights reserved.
//

#ifndef FlickrCoreData_PhotoDatabaseAvailability_h
#define FlickrCoreData_PhotoDatabaseAvailability_h

#define PhotoDatabaseAvailabilityNotification @"PhotoDatabaseAvailabilityNotification"
#define PhotoDatabaseAvailabilityContext @"Context"

#endif
